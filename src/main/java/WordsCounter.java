import java.util.*;

public class WordsCounter {
    private Set<String> keys = new HashSet<>();
    private Map<String, Integer> words = new TreeMap<>();

    public void finder(String text) {

        String delimiters = " \t\n\r\f,.!?—";

        StringTokenizer tokenizer = new StringTokenizer(text, delimiters);

        String word;
        Integer count;

        while (tokenizer.hasMoreTokens()) {
            word = tokenizer.nextToken().toLowerCase();
            count = words.get(word);
            count = (count == null) ? 1 : count + 1;
            words.put(word, count);
        }

        keys = words.keySet();

    }

    public void printResult() {
        for (String key : keys) {
            System.out.println(key + " : " + words.get(key));
        }
    }
}