import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class MyParser{

    public static void main(String[] args) throws FileNotFoundException {

        Parser parser = new Parser();
        WordsCounter counter = new WordsCounter();

        String parsedString = "";

        Scanner in = new Scanner(new File("src\\Mama.txt"));

        parsedString = parser.pars(in);

        counter.finder(parsedString);

        counter.printResult();
    }
}
